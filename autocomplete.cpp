/** Sean Castillo */

#include "DictionaryTrie.hpp"
#include <iostream>
#include <sstream>
#include <fstream>

using std::cout;
using std::cin;
using std::endl;
using std::ifstream;
using std::streamsize;
using std::streamoff;
using std::ios;
using std::ios_base;
using std::numeric_limits;

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        cout << "This program needs exactly one argument: "
             << "The name of the text file!" << endl;
        return -1;
    }

    // Open the file.
    cout << "Reading the file: " << argv[1] << endl;
    ifstream in;
    in.open(argv[1], ios::binary);

    // Check whether the file was opened.
    if (!in.is_open())
    {
        cout << "The input file is invalid. No file was opened. "
             << "Try again." << endl;
        return -1;
    }

    // Check for empty file.
    in.seekg(0, ios_base::end);
    streamoff len = in.tellg();
    if (len == 0)
    {
        cout << "The file is empty!" << endl;
        return -1;
    }

    // Reset the stream to the beginning of the file.
    in.seekg(0, ios_base::beg);


    // Build the trie.
    DictionaryTrie trie = DictionaryTrie();
    s_string line;

    while (!in.eof())
    {
        getline(in, line);
        if (!line.empty())
        {
            // Parse out every word in the line by space and store the words.
            s_string buff;
            std::stringstream ss(line);
            v_vector tokens;
            while (ss >> buff) { tokens.push_back(buff); }
            for (u_int i=0; i<tokens.size(); i++) { trie.insertWord(tokens[i]); }
        }
    }


    // Action Loop
    char response = 'y';
    while (tolower(response) == 'y')
    {
        s_string prefix = "";
        unsigned int completionCount;
        cout << "Enter a prefix to search:" << endl;
        cin >> prefix;

        cout << "Enter the number of completions:" << endl;
        while (!(cin>>completionCount))
        {
          cout << "Input invalid. Make it a number." << endl;
          cin.clear();
          cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
        cin.ignore();
        v_vector v;
        v = trie.completeWord(prefix, completionCount);
        for (v_vector::iterator it = v.begin(); it != v.end(); ++it)
        {
          cout << *it << endl;
        }

        cout << "Continue? (y/n)" << endl;
        cin >> response;
        cin.ignore();
    }
    if (in.is_open()) { in.close(); }
    return 0;
}
