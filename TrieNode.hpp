#ifndef TRIENODE_HPP
#define TRIENODE_HPPP
#include <iostream>
#include <iomanip>

class TrieNode
{
    public:
        /** Constructor initializes node with nullptrs and frequency of 0 */
        TrieNode(const char & c) : L(nullptr),    D(nullptr),
                                   R(nullptr),    P(nullptr),
                                   text(c), freq(0), endNode(false) {}

        TrieNode* L;
        TrieNode* D;
        TrieNode* R;
        TrieNode* P;
        const char text;
        unsigned int freq;
        bool endNode;
};

#endif  // TRIENODE_HPP