#ifndef DICTIONARY_TRIE_HPP
#define DICTIONARY_TRIE_HPP

#include <string>
#include <queue>
#include "TrieNode.hpp"

// Aliases for standard things
using u_int     = unsigned int;
using s_string  = std::string;
using v_vector  = std::vector<s_string>;
using pair_ui_s = std::pair<unsigned int, std::string>;
using pri_qu    = std::priority_queue<pair_ui_s>;

// Aliases for things specific to this program
using Node = TrieNode;
class DictionaryTrie
{
    protected:
        TrieNode* root;

    public:
        /* Create a dictionary */
        DictionaryTrie();

        /* Insert a word into the dictionary
         * Return true: Word inserted.
         * Return false: Word not inserted; if the input is valid,
         *               increment the word's count.
         */
        bool insertWord(s_string word);

        /* Return true:  Word is in the dictionary.
         * Return false: Word is not in the dictionary.
         */
        bool find(s_string word) const;

        v_vector completeWord(s_string prefix, u_int completionCount);

        /* Destructor */
        ~DictionaryTrie();

    private:
        /* Insert the node. A node contains a single char. */
        void insertNode(Node** curr, s_string word);

        /* The node insertion is done through recursion,
         * so this helper function allows insertWord() to
         * know whether to return true or false.
         * insertNode() sets this value because it can't
         * return a bool directly. */
        bool insertFlag;

        void assistCompleteWord(Node** n, pri_qu *pq, s_string s) const;


        /* Use this to check whether the tree is a single node. */
        bool isSingleNodeTree() const;

        /* Use this to check whether a node is the down child of some node. */
        bool isDownChild (Node* n) const;

        static void deleteEverything(Node* n);
};

#endif // DICTIONARY_TRIE_HPP