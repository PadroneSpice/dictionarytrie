#include "DictionaryTrie.hpp"

/* Create a new dictionary */
DictionaryTrie::DictionaryTrie()
{
    root = 0; //start with no root
    insertFlag = false;
}
/**
 * Insert a word into the dictionary.
 * Return true: Word inserted.
 * Return false: Word not inserted; if the input is valid,
 *               increment the word's count.
 */
 bool DictionaryTrie::insertWord(s_string w)
 {
     if (w.empty()) { return false; }           //empty string
     if (!root) { root = new Node(w[0]);}       //do this for first node

     insertFlag = false; //Reset the insertion flag.

     insertNode(&root, w);
     return insertFlag;
 }

void DictionaryTrie::insertNode(Node** curr, s_string w)
{
    //std::cout << "inserting: " << w << std::endl;
    if (w.empty()) { return; }

    // If letter is less, go left.
    if (w[0] < (*curr)->text)
    {
        if ((*curr)->L == nullptr)
        {
            Node* n = new Node(w[0]);
            ((*curr)->L = n);
            n->P = *curr;
        }
        insertNode( &(*curr)->L, w);
    }
    // If letter is greater, go right.
    else if (w[0] > (*curr)->text)
    {
        if ((*curr)->R == nullptr)
        {
            Node* n = new Node(w[0]);
            ((*curr)->R = n);
            n->P = *curr;
        }
        insertNode( &(*curr)->R, w);
    }
    // If letter matches, go down.
    else if (w[0] == (*curr)->text)
    {
        w.erase(0,1); //move to the next letter

        // If this is the end of the word, set the insertion flag
        // and increment the frequency, and then return.
        if (w.empty())
        {
            if ((*curr)->freq == 0) { insertFlag = true; }
            else                    { insertFlag = false;}
            (*curr)->freq += 1;               //increment frequency
            (*curr)->endNode = true;         //set the end node to true
            return;
        }

        // If this is not the end of the word, keep going down.

        //If the down child does not exist, make one.
        if ((*curr)->D == nullptr)
        {
            Node* n = new Node(w[0]);
            ((*curr)->D = n);
            n->P = *curr;
        }

        // Move to the down child.
        insertNode(&(*curr)->D, w);
    }
}

/* Return True: Word is in the dictionary. Return False: Otherwise. */
bool DictionaryTrie::find(std::string w) const
{
    if (!root) { return false; } // Edge case: No root.

    Node* curr = root;

    // Edge case: Single node tree
    // A single node tree has no children.
    // If the word is bigger than 1 char, then the word can't be here.
    // If the word is 1 char, compare it to the root's char.
    if (isSingleNodeTree())
    {
        if (w.size() > 1)       { return false; }
        if (w[0] == curr->text) { return true;  }
        return false;
    }

    // Now go through the word.
    // If a node required to travel does not exist, return false.
    s_string::iterator it = w.begin();
    while (it != w.end())
    {
        // If the letter is less than curr's letter, move left.
        if (*it < curr->text)
        {
            if (curr->L == nullptr) { return false; }
            curr = curr->L;
        }
        // If the letter is greater than curr's letter, move right.
        if (*it > curr->text)
        {
            if (curr->R == nullptr) { return false; }
            curr = curr->R;
        }
        // If a match is found, increment the iterator.
        // If this is the last letter and curr is an end node, return true.
        // If it's not the last letter, move down.
        //    --> If there's no down child, return false.
        else
        {
            ++it;
            if (it == w.end() && curr->endNode == true) { return true;}
            else
            {
                if (curr->D == nullptr) { return false; }
                curr = curr->D;
            }
        }
    }
    return false;
}

v_vector DictionaryTrie::completeWord(s_string prefix, u_int completionCount)
{
    v_vector v;     // vector of found words
    if (!root) { return v; } //Edge case: Empty Tree

    //Step 1: Find the prefix in the tree.
    Node* curr = root;

    pri_qu suziQ;   // frequency-order priority queue


    // Edge case: The tree has only one node.
    if (isSingleNodeTree())
    {
        // If the prefix is single-letter and there's a match,
        // push it into the vector and return.
        if (prefix.size() == 1 && prefix[0] == curr->text)
        {
            v.push_back(prefix);
        }
        return v;
    }

    s_string::iterator it = prefix.begin();
    while (it != prefix.end())
    {
        // Down
        if (*it == curr->text)
        {
            if (curr->D != nullptr)
            {
                if (++it == prefix.end()) { break; }
                else { curr = curr->D; }
            }
        
            else
            {
                if (++it == prefix.end()) { break; }
                else { --it; }
                return v;
            }
        }

        // Left
        else if ((*it < curr->text))
        {  
            if (curr->L != nullptr) { curr = curr->L; }
            else { return v; }
        }
        // Right
        else
        {
            if (curr->R != nullptr) { curr = curr->R; }
            else { return v; }
        }
    }

    /* Step 2: Run through all the words in the subtree.
     *         First, Add the prefix if it's a word.
     *         This uses the empty string because the "suffix" is the "prefix".
     */
    if (find(prefix)) { suziQ.emplace(curr->freq, ""); }

    /* Next, run assistCompleteWord().
     * It should start at the Down child of the last node of the prefix,
     * since that's where the word is completed in a trie. */
    if (curr->D != nullptr)
    {
        std::cout << "starting assistCompleteWord at node: " << curr->D->text << std::endl;
        assistCompleteWord(&(curr->D), &suziQ, "");
    }


    // Validate the completion size.
    if (completionCount > suziQ.size()) { completionCount = suziQ.size(); }

    while (!suziQ.empty() && completionCount > 0)
    {
        s_string suffix = suziQ.top().second;
        suffix.erase(0, 1); //cuts the first char from the completion, which
                            //is also the last char of the prefix
        std::cout << "pushing into vector: " << "prefix: " << prefix + " suffix: " << suffix << std::endl;
        v.push_back(prefix + suffix); // Insert the word into the vector.
        suziQ.pop();
        --completionCount;
    }

    return v;
}

/** Put the suffixes into a priority queue */
void DictionaryTrie::assistCompleteWord(Node** n, pri_qu *pq, s_string s) const
{
    if (*n != nullptr)
    {
        // If it's a down child, add the parent.
        if (isDownChild(*n))
        {
            s += (*n)->P->text;
            if ((*n)->endNode == true) //TODO: Why doesn't   if ((*n)->endNode)    work equally well here?
            {
                s += (*n)->text;
                std::cout << "end node found. s is now: " << s << std::endl;
                //std::cout << "putting in suziQ: " << (*n)->freq << " " << s << std::endl;
                pq->emplace((*n)->freq, s);
                s.pop_back(); // cuts the last char from the string build
                              // as to not double-append on the subsequent words
                //              //TODO: Think of a more elegant way of doing this.
            }
        }
        // Special Case: To words whose end nodes aren't down children
        else if ((*n)->P != nullptr && (*n)->endNode == true)
        {
            s += (*n)->text;             // Add this last node to the string.
            pq->emplace((*n)->freq, s);  // Place the build string.
            s.pop_back();                //Remove the last char from the built string.
        }

        if (((*n)->R) != nullptr) { assistCompleteWord(&((*n)->R), pq, s); }
        if (((*n)->D) != nullptr) { assistCompleteWord(&((*n)->D), pq, s); }
        if (((*n)->L) != nullptr) { assistCompleteWord(&((*n)->L), pq, s); }
    }
}





bool DictionaryTrie::isSingleNodeTree() const
{
    Node* n = root;
    if (n->D == nullptr && n->L == nullptr && n->R == nullptr)
    { return true; }
    return false;
}

bool DictionaryTrie::isDownChild(Node* n) const
{
    if (n->P &&             // 1. It has a parent.
        n->P->D &&          // 2. Its parent has a down child.
        n->P->D == n)       // 3. It is the down child.
    {
        return true;
    }
    else return false;
}

void DictionaryTrie::deleteEverything(Node* n)
{
    if (n == nullptr) { return; }
    deleteEverything(n->L);
    deleteEverything(n->D);
    deleteEverything(n->R);
    delete n;
}

/* Destructor */
DictionaryTrie::~DictionaryTrie() { deleteEverything(root); }
